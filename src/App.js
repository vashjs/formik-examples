import './App.css';
import {Routes, Route, Link} from "react-router-dom";
import BaseForm from "./components/BaseForm";
import UpdateForm from "./components/UpdateForm";
import CreateForm from "./components/CreateForm";

function App() {
  return (
    <div className="App">
        <div>
            <ul>
                <li><Link to="/"> Base </Link></li>
                <li><Link to="/create"> Create </Link></li>
                <li><Link to="/update"> Update </Link></li>
            </ul>
        </div>


        <div>
            <Routes>
                <Route path="/" element={<BaseForm />} />
                <Route path="create" element={<CreateForm />} />
                <Route path="update" element={<UpdateForm />} />
            </Routes>
        </div>
    </div>
  );
}

export default App;
