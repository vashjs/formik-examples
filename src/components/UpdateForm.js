import React from 'react';
import BaseForm from "./BaseForm";

const UpdateForm = () => {
    const handleSubmit = (values) => {
        alert(JSON.stringify(values, null, 2))
    }

    const initialValues = {
        firstName: "Name",
        lastName: "last name",
        email: "email@email.com",
        conditional: ''
    }

    return (
        <div>
            <h1>Update</h1>
            <BaseForm onSubmit={handleSubmit} initialValues={initialValues} />
        </div>
    );
};

export default UpdateForm;