import React, {useEffect} from 'react';
import {useFormik} from "formik";
import {ValidationSchema} from "./validation";

const baseFormValues = {
    firstName: '',
    lastName: '',
    email: '',
    conditional: ''
}

const BaseForm = ({onSubmit, initialValues}) => {
    const {values, handleSubmit, handleChange, dirty, errors, resetForm, isValid, touched, setErrors} = useFormik({
        initialValues: initialValues ? initialValues : baseFormValues,
        validationSchema: ValidationSchema,
        onSubmit,
        validateOnChange: false
    });

    useEffect(() => {
        setErrors({})
    }, [values])

    return (
       <>
           <form onSubmit={handleSubmit}>
               <hr/>
               <p>
                   <label htmlFor="firstName">First Name</label>
                   <input
                       id="firstName"
                       name="firstName"
                       type="text"
                       onChange={handleChange}
                       value={values.firstName}
                       className={`${(errors.firstName && touched.firstName) && 'invalid'}`}
                   />
                   {errors.firstName && touched.firstName ? <div>{errors.firstName}</div> : null}
               </p>

               <p>
                   <label htmlFor="lastName">Last Name</label>
                   <input
                       id="lastName"
                       name="lastName"
                       type="text"
                       onChange={handleChange}
                       value={values.lastName}
                       className={`${(errors.lastName && touched.lastName) && 'invalid'}`}
                   />
                   {errors.lastName && touched.lastName ? <div>{errors.lastName}</div> : null}
               </p>

               <p>
                   <label htmlFor="email">Email Address</label>
                   <input
                       id="email"
                       name="email"
                       type="text"
                       onChange={handleChange}
                       value={values.email}
                       className={`${(errors.email && touched.email) && 'invalid'}`}
                   />
                   {errors.email && touched.email ? <div>{errors.email}</div> : null}
               </p>

               {values.email && !errors.email && (
                   <p>
                       <label htmlFor="conditional">Conditional</label>
                       <input
                           id="conditional"
                           name="conditional"
                           type="text"
                           onChange={handleChange}
                           value={values.conditional}
                           className={`${(errors.conditional && !touched.conditional) && 'invalid'}`}
                       />
                       {errors.conditional ? <div>{errors.conditional}</div> : null}
                   </p>
               )}}

               <button type="submit" disabled={!dirty}>Submit</button>
           </form>

           <button type="button" onClick={resetForm}>Reset</button>
           <hr/>
           <pre><b>Values</b> {JSON.stringify(values, null, 2)}</pre>
           <hr/>
           <pre><b>Errors</b> {JSON.stringify(errors)}</pre>
           <hr/>
           <pre><b>Dirty</b> {JSON.stringify(dirty)}</pre>
           <pre><b>Is valid</b> {JSON.stringify(isValid)}</pre>
       </>
    );
};

export default BaseForm;