import React from 'react';
import BaseForm from "./BaseForm";

const CreateForm = () => {
    const handleSubmit = (values) => {
        alert(JSON.stringify(values, null, 2))
    }

    return (
        <div>
            <h1>Create</h1>
            <BaseForm onSubmit={handleSubmit} />
        </div>
    );
};

export default CreateForm;