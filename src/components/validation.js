import * as Yup from 'yup';

export const ValidationSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    conditional: Yup
        .string()
        .when("email", {
            is: 'admin@admin.com',
            then: Yup.string().required("Must fill this field because you are using admin@admin.com")
        })
});